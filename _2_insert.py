import pymongo

if __name__ == "__main__":
    client = pymongo.MongoClient("mongodb://localhost:27017/")
    db = client["pythonDb"]
    collection = db["sampleCollection"]

    # Insert One
    dbDict1 = {
        "name": "Siddarth",
                "age": 20,
                "city": "Banglore"
    }
    collection.insert_one(dbDict1)
    dbDict2 = {
        "name": "Mahesh",
        "age": 20,
        "city": "Andhra"
    }
    collection.insert_one(dbDict2)

    # Insert Many
    insertThese = [
        {
            "lang": "Python",
            "module": "pymongo"
        },
        {
            "lang": "Javascript",
            "module": "nodejs"
        },
        {
            "lang": "Java",
            "module": "spring"
        },
        {
            "lang": "C",
            "module": "stdio"
        },
        {
            "lang": "C++",
            "module": "stl"
        }
    ]
    collection.insert_many(insertThese)
