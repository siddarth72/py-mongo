import pymongo

if __name__ == "__main__":
    client = pymongo.MongoClient("mongodb://localhost:27017/")
    db = client["pythonDb"]
    collection = db["sampleCollection"]

    one = collection.find_one({'name': 'Mahesh'})
    print(one)
